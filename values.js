// create a function which work like a inbuile value function

function values(obj) {
    let valuesOfObject = [];

    for (let key in obj) {
        if (typeof (obj[key]) !== "function") {     // if value is not a function than push in array
            valuesOfObject.push(obj[key])
        }

    }

    return valuesOfObject
}

module.exports = values;



