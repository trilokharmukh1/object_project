// recreate pair method

function pairs(obj){
    let ansArray = [];

    for(let key in obj){
        ansArray.push([key,obj[key]])
    }

    return ansArray
}

module.exports = pairs;