// map method for object

function mapObject(obj, cb) {
    if(typeof(obj)!=="object"){
        return {};
    }
    
    newObject = {};

    for (let key in obj) {
        let val = obj[key];
        console.log(val);
        newObject[key] = cb(val);

    }
    return newObject;

}

module.exports = mapObject;
