//---this function work like a inbuilt defaults function

function defaults(obj, defaultProps) {
    if (typeof (obj) !== "object") {
        return {};
    }

    for (let key in defaultProps) {
        if (key in obj===false) {       // if key is undefined than add that properties on existing object
            obj[key]=defaultProps[key]
        }
    }
    return obj;
}

module.exports = defaults;