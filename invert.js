// recreate a invert object function

function invert(obj) {

    if (typeof (obj) !== "object") {
        return {};
    }

    invertObj = {};

    for (let key in obj) {
        invertObj[obj[key]] = key
    }

    return invertObj
}

module.exports = invert;